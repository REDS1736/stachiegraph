import codecs
import json
from os import remove
import sys

import pandas as pd

from python_gedcom_2.element.individual import IndividualElement
from python_gedcom_2.element.family import FamilyElement
from python_gedcom_2.parser import Parser


# TODO: BDATE format
# TODO: Umlaute

id_pointer_dict = {}  # pointer: id
pointer_id_dict = {}  # id: pointer


# UTILITY =====================================================================

def translate_date_gedcom2json(
    gedcom_date: str
):
    """Translate date: 17 JUN 2002 -> 2002-06-17T00:00:00.000Z"""
    if not gedcom_date:
        return ''

    day, month_str, year = '', '', ''
    date_split = gedcom_date.split(' ')
    if len(date_split) == 1:
        year = date_split[0]
    elif len(date_split) == 2:
        month_str, year = date_split
    elif len(date_split) == 3:
        day, month_str, year = date_split
    month = {
        'JAN': '01',
        'FEB': '02',
        'MAR': '03',
        'APR': '04',
        'MAY': '05',
        'JUN': '06',
        'JUL': '07',
        'AUG': '08',
        'SEP': '09',
        'OCT': '10',
        'NOV': '11',
        'DEC': '12',
    }.get(month_str, 'XX')
    json_date = '-'.join([year, month, day]) + 'T00:00:00.000Z'
    return json_date


def pointer_to_id(
    pointer: str
):
    if pointer not in pointer_id_dict.keys():
        new_id = 0
        if pointer_id_dict:
            new_id = max(id_pointer_dict.keys()) + 1
        pointer_id_dict[pointer] = new_id
        id_pointer_dict[new_id] = pointer
    return pointer_id_dict[pointer]


def born_after(
    node: dict,
    year: int,
    invalid: bool,
) -> bool:
    birth_year = None
    try:
        birth_year = int(node['birthDate'][:4])
    except:
        return invalid
    return birth_year > year


def link_is_valid(
    link: dict,
    nodes: list,
) -> bool:
    source_valid = any([
        node['id'] == link['source']
        for node
        in nodes
    ])
    target_valid = any([
        node['id'] == link['target']
        for node
        in nodes
    ])
    return (source_valid and target_valid)


# MAIN ========================================================================

def gedcom2json(
    input_filepath: str,
    output_filepath: str,
):
    # 1 READ FILE INTO PARSER -------------------------------------------------

    gedcom_parser = Parser()
    gedcom_parser.parse_file(input_filepath)

    elements = gedcom_parser.get_root_child_elements()


    # 2 PROCESS SINGLE HUMANS -------------------------------------------------

    humans_list = []
    for e in elements:
        if isinstance(e, IndividualElement):
            new_human = {
                'ID': pointer_to_id(e.get_pointer()),
                'NAME': ', '.join(e.get_name()[::-1]),
                'FAMC': '',   # Child of which family?
                'FAMS': '',   # Father / mother of which family?
                'BDATE': '',  # Birthday
                'lastName': e.get_name()[-1],
                'gender': e.get_gender(),
                'deathDate': translate_date_gedcom2json(e.get_death_data()[0]),
            }
            for attr in e.get_child_elements():
                if attr.get_tag() in (
                    'FAMC',
                    'FAMS'
                ):
                    new_human[attr.get_tag()] = attr.get_value()
                if attr.get_tag() == 'BIRT':
                    for b_attr in attr.get_child_elements():
                        if b_attr.get_tag() == 'DATE':
                            new_human['BDATE'] = translate_date_gedcom2json(
                                b_attr.get_value()
                            )

            humans_list.append(new_human)

    humans_dict = dict()
    for human in humans_list:
        humans_dict[human['ID']] = human


    # 3 PROCESS SINGLE FAMILIES -----------------------------------------------R

    families_list = []
    for e in elements:
        if isinstance(e, FamilyElement):
            new_family = {
                'ID': e.get_pointer(),
                'HUSB': '',
                'WIFE': '',
                'CHILDREN': [],
            }
            for attr in e.get_child_elements():
                if attr.get_tag() == 'CHIL':
                    new_family['CHILDREN'].append(
                        pointer_to_id(attr.get_value())
                    )
                if attr.get_tag() in (
                    'HUSB',
                    'WIFE',
                ):
                    new_family[attr.get_tag()] = pointer_to_id(attr.get_value())

            families_list.append(new_family)

    families_dict = dict()
    for family in families_list:
        families_dict[family['ID']] = family


    # 4 LINK HUMANS -----------------------------------------------------------

    links = []

    for family in families_list:
        if family['HUSB'] and family['WIFE']:
            humans_dict[family['HUSB']]['spouseId'] = family['WIFE']
            humans_dict[family['WIFE']]['spouseId'] = family['HUSB']
            links.append({
                'source': family['HUSB'],
                'target': family['WIFE'],
                'relation': 'spouse',
            })

        if family['CHILDREN']:
            for child in family['CHILDREN']:
                humans_dict[child]['fatherId'] = family['HUSB']
                humans_dict[child]['motherId'] = family['WIFE']
                if family['HUSB'] != '':
                    links.append({
                        'source': child,
                        'target': family['HUSB'],
                        'relation': 'father',
                    })
                if family['WIFE'] != '':
                    links.append({
                        'source': child,
                        'target': family['WIFE'],
                        'relation': 'mother',
                    })

    humans_linkedlist = []
    for key, attributes in humans_dict.items():
        new_human = {
            'id': key,
            'name': attributes.get('NAME', ''),
            'fatherId': attributes.get('fatherId', ''),
            'motherId': attributes.get('motherId', ''),
            'spouseId': attributes.get('spouseId', ''),
            'gender': attributes.get('gender', ''),
            'birthDate': attributes.get('BDATE', ''),
            'deathDate': attributes.get('deathDate', ''),
            'lastName': attributes.get('lastName', ''),
        }
        humans_linkedlist.append(new_human)

    with codecs.open(
        filename=output_filepath,
        mode='w',
        encoding='UTF-8'
    ) as f:
        json.dump(
            obj={
                'nodes': humans_linkedlist,
                'links': links,
            },
            fp=f,
            indent=4,
        )


def filter_json(
    input_filepath: str,
    output_filepath: str,
    remove_born_after: int,
    invalid_birthyear: bool,
):
    """Filter JSON
    :param input_filepath: path to input file
    :param output_filepath: path to input file
    :param remove_born_after: Remove all nodes that were born after this year
    :param invalid_birthyear: Remove nodes with missing / invalid birth years?
    """
    # Input ...............................................
    with open(input_filepath, encoding='utf-8') as f:
        json_dict = json.load(f)

    # remove_born_after ...................................
    if remove_born_after is not None:
        json_dict['nodes'] = [
            node
            for node
            in json_dict['nodes']
            if not born_after(
                node=node,
                year=remove_born_after,
                invalid=invalid_birthyear
            )
        ]

    # Cleanup links .......................................
    json_dict['links'] = [
        link
        for link
        in json_dict['links']
        if link_is_valid(
            link,
            json_dict['nodes'],
        )
    ]

    # Output ..............................................
    with open(output_filepath, 'w+', encoding='utf-8') as f:
        json.dump(
            obj=json_dict,
            fp=f,
            indent=4,
        )
