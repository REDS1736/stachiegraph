import json


FILEPATH = 'data/familyData2.json'


def main():
    with open(FILEPATH) as f:
        content = json.load(f)

    node_id_list = []
    for node in content['nodes']:
        if node['id'] not in node_id_list:
            node_id_list.append(node['id'])

    links = content['links']
    counter = 0
    for link in links:
        if not (
            link['source'] in node_id_list and
            link['target'] in node_id_list
        ):
            counter += 1
            print('invalid link!')
            print(json.dumps(link, indent=4))
            print('----------------------')
    print()
    print(f"{counter} / {len(links)} are invalid")


if __name__ == '__main__':
    main()