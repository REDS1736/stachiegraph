from django.http import HttpResponse
from django.shortcuts import render

import datafile_handling

import os


def converter(request):
    # POST ----------------------------------------------------------
    if request.method == 'POST':
        # Gedcom -> Json ..................................
        gedcom_content = request.FILES.get('jsonupload').read().decode('utf-8')
        with open('temp/temp.ged', 'w+', encoding='utf-8') as f:
            f.write(gedcom_content.replace('\r\n', '\n'))
        datafile_handling.gedcom2json(
            input_filepath='temp/temp.ged',
            output_filepath='temp/temp.json',
        )

        # Filter json .....................................
        with open('temp/temp.json', encoding='utf-8') as f:
            json_content = f.read()

        filter_birthyear = None
        try:
            filter_birthyear = int(request.POST.get('filter-birthyear', None))
        except:
            filter_birthyear = None

        invalid_birthyear = request.POST.get(
            'filter-birthyear-invalid', None
        ) == 'on'

        datafile_handling.filter_json(
            'temp/temp.json',
            'temp/temp-filtered.json',
            remove_born_after=filter_birthyear,
            invalid_birthyear=invalid_birthyear,
        )

        # Json -> response ................................
        with open('temp/temp-filtered.json', encoding='utf-8') as f:
            json_content = f.read()

        response = HttpResponse(
            json_content,
            content_type='application/force-download',
        )
        response['Content-Disposition'] = 'attachment; filename=yourdata.json'

        # Delete temp files ...............................
        os.remove('temp/temp.ged')
        os.remove('temp/temp.json')
        os.remove('temp/temp-filtered.json')

        return response

    # GET -----------------------------------------------------------
    else:
        return render(request, 'converter/converter.html')
