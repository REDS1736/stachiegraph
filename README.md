# StachieGraph

Visualizing geneological data does not require you to be a programmer!

StachieGraph is based on this great project:
https://github.com/bengarvey/lineage


## Setup / Installation

**Note:**
This guide is only meant for *local* hosting.
It is optimized to be simple and little effort.
If you want to set up a web server open to the internet, i strongly advise you
to take appropriate security measures.

StachieGraph is basically a server that serves a web site which you will visit
using the browser of your choice.
This web site is only supposed to be served *locally*.
This means that per default, you can only visit it if you're on the same network
(e.g. WiFi network) as the computer on which the server is running.
Of course, you can simply run the server on the same computer that you're using
to visit the web site.
That's how i do it.

### 1 - Prerequisites

The StachieGraph web server is implemented in
[Python](https://www.python.org)
using the
[Django](https://www.djangoproject.com/)
framework.
Installing Django is done by
[pip](https://pip.pypa.io/en/stable/),
the standard package manager for Python.
Let's get started installing these three things on your machine!

#### 1.1 - Python

This project requires Python version 3.7 or higher.
I personally use version 3.9.7 for development.

To find out whether you have a working install of Python 3.x on your machine,
type `python3 --version` in your terminal and execute it.
This should print the version of Python you have installed.
Depending on how you installed it / configured your system, Python might not be
found as `python3` but instead as `python`.
So if you think you installed Python correctly but for some reason
`python3 --version` does not print the expected output, try `python --version`.
If this command prints a Python 3.x version, you're good to go!

*Linux:*
Update your system.
You already have Python 3.x installed.

*Windows:*
Download an installer for 3.7 or higher
[here](https://www.python.org/downloads/)
and run it.

*MacOS:*
I think, current versions of MacOS come with Python 3.x pre-installed.
I need to check that though.

#### 1.2 - Pip

*Linux:*
Installation depends on your distro.
Most likely, somebody has already packaged pip for your distro so that you can
simply install it like any other package you install using `apt`, `pacman`,
`dnf` or whatever your distro is using.

*Windows:*
Surprise:
The installer you ran in the previous step installs not only python but pip too.
Easy-peasy, you may move on to the next step.

*MacOS:*
Apparently, you're supposed to install pip by running this command (which i
haven't tried yet...):
`curl https://bootstrap.pypa.io/get-pip.py | python`

#### 1.3 - Django

Ah, the beauty of pip:
This installation is the same on all systems.
Type `pip install Django` in your terminal and wait a few seconds until
it is installed.

### 2 - Installing StachieGraph

Download the source files either as a
[zip](https://codeberg.org/REDS1736/stachiegraph/archive/main.zip)
archive (for Windows users) or as a
[tar.gz](https://codeberg.org/REDS1736/stachiegraph/archive/main.tar.gz)
archive (for Linux and MacOS) users.

Unpack the archive you downloaded at some location on your machine.
As example locations for the rest of this guide i will use
`C:\Users\Guido\Desktop\stachiegraph`
for Windows and
`/home/guido/desktop/stachiegraph`
for Linux and MacOS.

If you know git, you may of course clone the repo to your desired location,
but as StachieGraph is targeted towards non-programmers, i don't expect
everybody to use git.

### 3 - Running the server

Open up a terminal in the directory where you installed StachieGraph and
execute
`python3 manage.py runserver` .
You can then access StachieGraph by visiting `127.0.0.1:8000` in your browser.


## Formatting your data

Names in your dataset may not contain the following characters:
- `~!*()'` (removed by `tree.js > encodeForPermalink()`)
- `:` (used to separate names from colors in color palette URI representations)


## Configuration
... happens in `config.json`!
The following keys are currently implemented:

### `dataPath`
This is where the JSON file containing your genealogical data is located.
Path is absolute to project root (`manage.py`).
Must be present and valid, otherwise the server fails.

### `defaultFilter`
You know the "filter" inputfield in the UI?
This fields value will be set to the value of `defaultFilter` by default.
It can still be changed manually from then.
Defaults to `""` (no filter).

### `minYear`
The earliest / first / oldest year that can be viewed.
Only allows numbers.
Defaults to `1950`.

### `maxYear`
The latest / last year that can be viewed.
Only allows numbers.
Defaults to `2022`.

### `showDead`
If `false`, show only people who are alive at the time of the current year.
If `true`, show all people who have been born at the time of the current year
(including those who have already died).
Only allows booleans.
Defaults to `true`.

### `maxLifeTime`
If the death date of a person is unknown, assume they're dead after
`maxLifeTime` years (and thus, remove them from the tree graph, if `showDead` is
`false`).
Only allows numbers.
Defaults to `120`.

### `defaultZoomLevel`
Zoom level of the graph:
Higher number means stronger zoom (makes everything larger).
Only allows numbers.
Defaults to `36`.

### `nodeRadius`
Size of individual nodes defined as their radius.
Only allows numbers.
Defaults to `5`.


### `linkDistance`
How long are the links between connected nodes?
Only allows numbers.
Defaults to `30`.


### `yearsPerSecond`
How many years for the graph shall pass in one second of your time?
Only allows numbers.
Defaults to `5`.


### `nodeBgColor`
Each node gets a slightly larger background in this color.
The color will wash out over time, until it is completely transparent when the
person is `nodeBgMaxAge` years old.
Only allows colors in RGB hex format (e.g. `#99bbff`).
Defaults to `""` (draw no background).


### `nodeBgMaxAge`
At which age will a nodes background have faded out of existence?
Only relevant if `nodeBgColor` is not empty.
Only allows numbers.
Defaults to `20`.


### `nodeBgRadius`
Radius of the node background.
Only relevant if `nodeBgColor` is not empty.
Only allows numbers.
Defaults to `15`.


### `nodesColorPaletteDisplayOnlyActive`
Default value for the "Display only active colors" checkbox for the nodes color
palette.
Only allows booleans.
Defaults to `true`.


### `graphBgColor`
Background color for the graph canvas.
Only allows colors in RGB hex format (e.g. `#ffffff`).
Defaults to `#ffffff` (white background).


## ToDo
- Linking lines in tree graph need a direction (arrow head?)
- New django app: gedcom to json converter