from django.shortcuts import render
import json

from secret import CONFIG_PATH


# UTILITY ======================================================================

def load_config():
    with open(CONFIG_PATH) as f:
        config = json.load(f)
    return config


# VIEWS ========================================================================

def membercount_graph(request):
    config = load_config()
    data_path = config.get('dataPath')
    this_config = config.get('apps').get('membercount')

    with open(data_path, encoding='utf-8') as f:
        data = json.load(f)

    args = {
        'data': json.dumps(data),
        'config_interestingLastNames': this_config.get('interestingLastNames', ''),
        'config_minYear': this_config.get('minYear', ''),
        'config_maxYear': this_config.get('maxYear', ''),
    }
    return render(request, 'graphs/membercount.html', args)


def pyramid_graph(request):
    config = load_config()
    data_path = config.get('dataPath')

    with open(data_path, encoding='utf-8') as f:
        data = json.load(f)

    args = {
        'data': json.dumps(data),
    }
    return render(request, 'graphs/pyramid.html', args)


def tadpole_graph(request):
    config = load_config()
    data_path = config.get('dataPath')
    this_config = config.get('apps').get('tadpole')

    with open(data_path, encoding='utf-8') as f:
        data = json.load(f)

    args = {
        'data': json.dumps(data),
        'config_interestingLastNames': this_config.get('interestingLastNames', ''),
        'config_minYear': this_config.get('minYear', ''),
        'config_maxYear': this_config.get('maxYear', ''),
        'config_updateRange': this_config.get('updateRange', ''),
    }
    return render(request, 'graphs/tadpole.html', args)


def tree_graph(request):
    config = load_config()
    data_path = config.get('dataPath')
    this_config = config.get('apps').get('tree')

    with open(data_path, encoding='utf-8') as f:
        data = json.load(f)

    args = {
        'data': json.dumps(data),
        'config_defaultFilter': this_config.get('defaultFilter', ''),
        'config_minYear': this_config.get('minYear', ''),
        'config_maxYear': this_config.get('maxYear', ''),
        'config_showDead': this_config.get('showDead', ''),
        'config_maxLifeTime': this_config.get('maxLifeTime', ''),
        'config_defaultZoomLevel': this_config.get('defaultZoomLevel', ''),
        'config_nodeRadius': this_config.get('nodeRadius', ''),
        'config_linkDistance': this_config.get('linkDistance', ''),
        'config_yearsPerSecond': this_config.get('yearsPerSecond', ''),
        'config_nodeBgColor': this_config.get('nodeBgColor', ''),
        'config_nodeBgMaxAge': this_config.get('nodeBgMaxAge', ''),
        'config_nodeBgRadius': this_config.get('nodeBgRadius', ''),
        'config_nodesColorPaletteDisplayOnlyActive':
            this_config.get('nodesColorPaletteDisplayOnlyActive', ''),
        'config_graphBgColor': this_config.get('graphBgColor', ''),
    }

    return render(request, 'graphs/tree.html', args)
