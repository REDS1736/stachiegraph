from django.urls import path

from .views import (
    membercount_graph,
    pyramid_graph,
    tadpole_graph,
    tree_graph,
)


urlpatterns = [
    path('membercount/', membercount_graph),
    path('pyramid/', pyramid_graph),
    path('tadpole/', tadpole_graph),
    path('tree/', tree_graph),
]
