function BarPlot(elem) {
    /**
     * Life cycle:
     *      bp = new BarPlot(elem)
     *      bp.setMargin(t, b, l, r)
     *      bp.setPixelDimensions(w, h)
     *      bp.setData(data)
     *      bp.setDomainX(domain)
     *      bp.setDomainY(min, max)
     *      bp.initPlot()
     */
    function bp() {}

    // VARIABLES ===============================================================

    // Settings -----------------------------------------------------

    let margin = {
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    };

    let size = {
        width: 0,
        height: 0,
    }

    let events = {
        barMouseOver: null,
        barMouseOut: null,
    }

    let barWidth = 50;

    let animationDuration = 200;

    let data = null;


    // Components ---------------------------------------------------

    let svg = elem;

    let domains = {
        x: null,
        y: null,
    }

    let scales = {
        x: null,
        y: null,
    }

    let axes = {
        x: null,
        y: [
            null,
            null,
        ]
    }


    // FUNCTIONS ===============================================================

    // Get / Set ----------------------------------------------------

    bp.setData = (data_) => data = data_;

    bp.setBarMouseOver = (e) => events.barMouseOver = e;
    bp.setBarMouseOut = (e) => events.barMouseOut = e;

    bp.setMargin = (top, bottom, left, right) => {
        margin.top = top;
        margin.bottom = bottom;
        margin.left = left;
        margin.right = right;
    }


    bp.setPixelDimensions = (w, h) => {
        size.width = w;
        size.height = h;

        svg
            .attr('width', size.width)
            .attr('height', size.height)
    }


    // Plot ---------------------------------------------------------

    bp.setDomainX = (domain) => {
        domains.x = domain;

        if (scales.x != null) {
            scales.x.domain(domain);

            svg.html(' ')
            bp.initPlot()
        }
    }

    bp.setDomainY = (min, max) => {
        domains.y = [min, max];

        if (scales.y != null)
            scales.y.domain(domains.y);
    }

    /**
     * `plotData` is a list of objects of shape `{ x: 'blabla', y: 123 }`
     */
    bp.initPlot = () => {
        let thisData = data.filter((d) => domains.x.includes(d.x))

        size.width = (thisData.length * barWidth) + margin.left + margin.right;

        // X axis ...............................
        scales.x = d3.scaleBand()
            .range([
                margin.left,
                size.width - margin.right,
            ])
            .padding(0.1)
            .domain(domains.x)

        // Y axis ...............................
        scales.y = d3.scaleLinear()
            .range([
                size.height - margin.bottom,
                margin.top,
            ])
            .domain(domains.y)

        // Draw .................................
        svg
            .selectAll('rect')
            .data(thisData)
            .join('rect')
            .on('mouseover',events.barMouseOver)
            .on('mouseout', events.barMouseOut)
            .attr('class', 'bar')
            .attr('x', (d) => scales.x(d.x))
            .attr('y', (d) => scales.y(d.y))
            .attr('raw', (d) => d.y)
            .attr('width', scales.x.bandwidth())
            .attr('height', (d) => size.height - margin.bottom - scales.y(d.y))

        axes.x = svg
            .append('g')
            .attr('transform', `translate(0, ${size.height - margin.bottom})`)
            .call(d3.axisBottom(scales.x))
            .style('text-anchor', 'end')
            .selectAll('text')
            .attr('dx', '-.8em')
            .attr('dy', '.15em')
            .attr('transform', 'rotate(-65)')

        axes.y = svg
            .append('g')
            .attr('transform', `translate(${margin.left}, 0)`)
            .call(d3.axisLeft(scales.y))
    }


    bp.updatePlot = () => {
        thisData = data.filter((d) => domains.x.includes(d.x))

        svg
            .selectAll('rect')
            .data(thisData)
            .transition()
            .duration(animationDuration)
            .attr('x', (d) => scales.x(d.x))
            .attr('y', (d) => scales.y(d.y))
            .attr('raw', (d) => d.y)
            .attr('width', scales.x.bandwidth())
            .attr('height', (d) => size.height - margin.bottom - scales.y(d.y))
    }


    return bp;
}


function TadpolePlot(elem) {
    function tpp() {}

    // VARIABLES ==============================================================

    // Settings -----------------------------------------------------

    let margin = {
        left: 50,
        right: 100,
    }

    let size = {
        width: 0,
        height: 0,
    }

    let detailsOffset = {
        x: 20,
        y: 20,
    }

    let data = null;

    let nodeRadius = 5;

    let minYear = null;
    let maxYear = null;

    let alphaTarget = 1;

    let colorPalette = [];


    // Components ---------------------------------------------------

    let canvas = elem;
    let context = elem.node().getContext('2d');
    let simulation = null;
    let detailsDiv = null;


    // FUNCTIONS ==============================================================

    // Get / Set ----------------------------------------------------

    tpp.setAlphaTarget = (at) => alphaTarget = at;

    tpp.setDetailsDiv = (dd) => detailsDiv = dd;

    tpp.setData = (data_) => {
        data = data_;
    }
    tpp.getData = () => data;

    tpp.setRange = (minYear_, maxYear_) => {
        minYear = minYear_;
        maxYear = maxYear_;
    }

    tpp.setMargin = (left, right) => {
        margin.left = left;
        margin.right = right;
    }

    tpp.setPixelDimensions = (w, h) => {
        size.width = w;
        size.height = h;

        canvas
            .attr('width', size.width)
            .attr('height', size.height)
    }

    tpp.setColorPalette = (colorPalette_) => {
        colorPalette = colorPalette_;
    }
    tpp.getColorPalette = () => colorPalette;

    // Utility ------------------------------------------------------

    function xCenter(node) { let pos01 = (node.birthYear - minYear) / (maxYear - minYear);
        let availableWidth = size.width - (margin.left + margin.right);
        let pos = margin.left + (pos01 * availableWidth);
        return pos;
    }


    // Plot ---------------------------------------------------------

    tpp.initPlot = () => {
        simulation = d3.forceSimulation(data)
            .force('charge', d3.forceManyBody().strength(1))
            .force('x', d3.forceX().x((node) => xCenter(node)))
            //.force('y', d3.forceY().y(size.height / 2))
            .force('collision', d3.forceCollide().radius(nodeRadius))
            .alphaTarget(alphaTarget)
            .on('tick', ticked);

        canvas
            .on('mousemove', onMouseMove)
    }


    tpp.updatePlot = (data_) => {
        data = data_;

        simulation
            .nodes(data)
            .alphaTarget(alphaTarget)
            .restart();
    }


    function ticked() {
        context.rect(
            0, 0,
            size.width, size.height,
        );
        context.fillStyle = '#ffffff';
        context.fill();

        canvas
            .selectAll('circle')
            .data(data)
            .join('circle')
            .attr('r', nodeRadius)
            .attr('cx', (d) => d.x)
            .attr('cy', (d) => d.y)

        data.forEach((node) => {
            drawNode(node);
        });
    }


    function drawNode(node) {
        context.beginPath();
        context.arc(
            node.x,
            node.y,
            nodeRadius,
            0, 2 * Math.PI,
        );
        context.fillStyle = colorPalette[node.lastName];
        //context.fillStyle = '#ffee22';
        context.fill();
    }


    // Interaction --------------------------------------------------

    function onMouseMove(event) {
        var mouse = d3.pointer(event);
        var node = simulation.find(
            mouse[0],
            mouse[1],
            nodeRadius,
        );
        if (!node) hideNodeDetails();
        else displayNodeDetails(node, mouse)
    }

    function hideNodeDetails() {
        detailsDiv.css('display', 'none');
    }

    function displayNodeDetails(node, mouse) {
        detailsDiv.css({
            'display': 'block',
            'left': (mouse[0] + detailsOffset.x)  + 'px',
            'top': (mouse[1] + detailsOffset.y)  + 'px',
        })
        detailsDiv.find('#node-details-name').html(node.name)
        detailsDiv.find('#node-details-birthdate').html(node.birthDate)

        // Flip details to the left side of cursor if it's too close to right
        // border of the screen
        if (mouse[0] + detailsOffset.x + detailsDiv.width() > window.innerWidth) {
            detailsDiv.css(
                'left',
                detailsDiv
                    .css('left')
                    .substring(0, detailsDiv.css('left').indexOf('px'))
                    - detailsDiv.width()
                    + 'px'
            )
        }
    }


    return tpp;
}


function PyramidPlot(elem) {
    function pp() {}

    // VARIABLES ==============================================================

    // Settings -----------------------------------------------------

    let margin = {
        top: 100,
        bottom: 100,
        left: 100,
        right: 100,
    }

    let size = {
        width: 0,
        height: 0,
    }

    let binCount = 10;

    // Data ---------------------------------------------------------

    let data = null;

    let bins = {
        m: null,
        f: null,
    };

    // Components ---------------------------------------------------

    let svg = elem;

    let domains = {
        x: null,
        y: null,
    }

    let scales = {
        xPositive: null,
        xNegative: null,
        y: null,
    }

    let axes = {
        xPositive: null,
        xNegative: null,
        y: null,
    }

    let detailsDiv = null;


    // FUNCTIONS ==============================================================

    // Get / Set ----------------------------------------------------

    pp.setData = (data_) => data = data_;
    pp.getData = () => data;

    pp.setDomainY = (min, max) => domains.y = [min, max];
    pp.setDomainX = (min, max) => {
        domains.x = [
            min,
            max,
        ]
    }

    pp.setBinCount = (bc) => binCount = bc;

    pp.getBins = () => bins;

    pp.setMargin = (top, bottom, left, right) => {
        margin.top = top;
        margin.bottom = bottom;
        margin.left = left;
        margin.right = right;
    }

    pp.setPixelDimensions = (w, h) => {
        size.width = w;
        size.height = h;

        svg
            .attr('width', size.width)
            .attr('height', size.height)
    }

    pp.setDetailsDiv = (dd) => detailsDiv = dd;


    // Utility ------------------------------------------------------

    // Plot ---------------------------------------------------------
    pp.initPlot = () => {
        let dataM = data.filter((n) => n.gender == 'M')
        let dataF = data.filter((n) => n.gender == 'F')

        // Y axis ................................
        scales.y = d3.scaleLinear()
            .domain(domains.y)
            .range([
                size.height - margin.bottom,
                margin.top
            ])

        // Histogram .............................
        let histogram = d3.histogram()
            .value((d) => d.age)
            .domain(domains.y)
            .thresholds(scales.y.ticks(binCount))  // number of bins

        bins.m = histogram(dataM);
        bins.f = histogram(dataF);

        // X axis ................................
        scales.xPositive = d3.scaleLinear()
            .domain(domains.x)
            .range([
                width / 2,
                size.width - margin.right,
            ])
        scales.xNegative = d3.scaleLinear()
            .domain(domains.x)
            .range([
                width / 2,
                margin.left,
            ])

        let xCenter = domains.x[0] + ((domains.x[1] - domains.x[0]) / 2);
        svg.append("text")
            .attr("class", "x label")
            .attr("text-anchor", "middle")
            .attr("x", scales.xPositive(xCenter))
            .attr("y", margin.top)
            .text("men")
        svg.append("text")
            .attr("class", "x label")
            .attr("text-anchor", "middle")
            .attr("x", scales.xNegative(xCenter))
            .attr("y", margin.top)
            .text("women")

        // Draw ..................................
        svg.selectAll("rect.bar.m")
            .data(bins.m)
            .join("rect")
            .on('mouseover', mouseOver)
            .on('mouseout', mouseOut)
            .attr('class', (d) => 'bar m')
            .attr('raw', (d) => d.length)
            .attr("transform", (d) => `translate(${size.width / 2}, ${scales.y(d.x1)})`)
            .attr("height", (d) => scales.y(d.x0) - scales.y(d.x1) - 1)
            .attr("width", (d) => scales.xPositive(d.length) - (size.width / 2))

        svg.selectAll("rect.bar.f")
            .data(bins.f)
            .join("rect")
            .on('mouseover', mouseOver)
            .on('mouseout', mouseOut)
            .attr('class', (d) => 'bar f')
            .attr('raw', (d) => d.length)
            .attr("transform", (d) => `translate(${scales.xPositive(-d.length)}, ${scales.y(d.x1)})`)
            .attr("height", (d) => scales.y(d.x0) - scales.y(d.x1) - 1)
            .attr("width", (d) => scales.xPositive(d.length) - (size.width / 2))

        axes.xPositive = svg
            .append("g")
            .attr("transform", `translate(0, ${size.height - margin.bottom})`)
            .call(d3.axisBottom(scales.xPositive))
        axes.xNegative = svg
            .append("g")
            .attr("transform", `translate(0, ${size.height - margin.bottom})`)
            .call(d3.axisBottom(scales.xNegative))

        axes.y = svg
            .append('g')
            .attr('transform', `translate(${margin.left}, 0)`)
            .call(d3.axisLeft(scales.y))
    }


    pp.updatePlot = (data_) => {
        data = data_;
        let dataM = data.filter((n) => n.gender == 'M')
        let dataF = data.filter((n) => n.gender == 'F')

        // Histogram .............................
        let histogram = d3.histogram()
            .value((d) => d.age)
            .domain(domains.y)
            .thresholds(scales.y.ticks(binCount))  // number of bins

        bins.m = histogram(dataM);
        bins.f = histogram(dataF);

        // Draw ..................................
        svg.selectAll("rect.bar.m")
            .data(bins.m)
            .transition()
            .duration(200)
            .attr('raw', (d) => d.length)
            .attr("width", (d) => scales.xPositive(d.length) - (size.width / 2))

        svg.selectAll("rect.bar.f")
            .data(bins.f)
            .transition()
            .duration(200)
            .attr('raw', (d) => d.length)
            .attr("transform", (d) => `translate(${scales.xPositive(-d.length)}, ${scales.y(d.x1)})`)
            .attr("width", (d) => scales.xPositive(d.length) - (size.width / 2))
    }


    // Interaction --------------------------------------------------

    function mouseOver(event) {
        let bbox = event.target.getBoundingClientRect();
        let bar = $(event.target)

        if (bar.attr('class').includes('m')) {
            detailsDiv.css({
                'left': bbox.right,
                'right': 'auto',
                'border-left': '2px solid black',
                'border-right': 'none',
            })
        } else {
            detailsDiv.css({
                'right': $(window).width() - bar.offset().left,
                'left': 'auto',
                'border-right': '2px solid black',
                'border-left': 'none',
            })
        }
        detailsDiv.css({
            'display': 'flex',
            'top': bbox.top,
            'height': bbox.height,
        })
        detailsDiv.html(Math.round(bar.attr('raw')))
    }

    function mouseOut(event) {
        detailsDiv.css('display', 'none');
    }


    return pp;
}