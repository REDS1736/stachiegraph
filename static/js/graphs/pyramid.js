// SETTINGS ====================================================================

const loopDuration = 200;

let width = 500;
let height = 600;

// With config keys .......................................

let interestingLastNames = [];
if (config_interestingLastNames != '')
    interestingLastNames = config_interestingLastNames
        .split(' ')
        .filter((i) => i.length > 0);

let minYear = 1800;
if (config_minYear != null)
    minYear = config_minYear;

let maxYear = 2022;
if (config_maxYear != null)
    maxYear = config_maxYear;


// VARIABLES ==================================================================

// Graph ..................................................
let pp = null;

// Data ...................................................
let allNodes = null;
let nodes = null;

// DOM ....................................................
let svg = null;
let tools = null;
let detailsDiv = null;
let yearDiv = null;

// State ..................................................
let isPlaying = true;
let year = null;
let refreshGraph = false;


// MAIN =======================================================================

init();


function init() {
    // Init data ..........................................
    year = minYear;
    allNodes = prepareData(transportData)

    nodes = allNodes
    if (interestingLastNames.length > 0)
        nodes = allNodes.filter((node) => 
            interestingLastNames.includes(node.lastName)
        )

    // Get DOM variables ..................................
    svg = d3.select('#graph');
    detailsDiv = $('#details')
    yearDiv = $('#year');
    tools = $('#tools');

    // Init DOM ...........................................

    // Init plot ..........................................
    pp = PyramidPlot(svg)
    pp.setData(calcAges(nodes, year))
    pp.setMargin(50, 50, 50, 50)
    pp.setPixelDimensions(width, height)
    pp.setDetailsDiv(detailsDiv)
    pp.setDomainX(0, 60)
    pp.setDomainY(0, 100)
    pp.setBinCount(20)
    pp.initPlot();

    // Start loop .........................................
    loop();
    d3.interval(
        loop,
        loopDuration,
        d3.now()
    )
}


function loop() {
    if (isPlaying) {
        lastYear = year;
        year = Math.min(maxYear, year + 1);
        yearDiv.html(year);
    }

    if (year != lastYear || refreshGraph) {
        pp.updatePlot(
            calcAges(nodes, year)
        );
        refreshGraph = false;
    }
}


// UTILITY ====================================================================

/**
 * Translate object of shape `{ a: aa, b: bb, c: cc }` into array of shape
 * `[{ x: a, y: aa }, { x: b, y: bb }, { x: c, y: cc }]`.
 * 
 * @param {object} dict
 * @returns {object[]}
 */
function dictToArray(dict) {
    let out = [];
    Object.keys(dict).forEach((key) => {
        out.push({
            'x': key,
            'y': dict[key]
        })
    })
    return out.sort((a, b) => a.x > b.x);
}


// DATA PROCESSING ============================================================

/** * Is `value` unique in `list`?
 *
 * @param {*} value 
 * @param {int} index 
 * @param {*[]} list
 * @returns {boolean}
 */
function unique(value, index, list) { 
    return list.indexOf(value) === index;
}


function date2year(dateString) {
    if (
        dateString == undefined ||
        dateString == null
    )
        return null;
    if (dateString.length < 4)
        return null;

    var out = Number(dateString.substring(0, 4));
    if (isNaN(out))
        return null;
    return out;
}


function prepareData(dataInput) {
    out = dataInput['nodes'];

    out.forEach((node) => {
        node.birthYear = date2year(node.birthDate);
        node.deathYear = date2year(node.deathDate);
    });

    out = out.filter((n) => (n.birthYear != null) && (n.deathYear != null));

    return out;
}


/**
 * Calculate `age` attribute for each node.
 * Age is negative if node is not alive.
 * 
 * @param {Object[]} nodes_ 
 * @param {int}} year_ 
 * @returns 
 */
function calcAges(nodes_, year_) {
    nodes_.forEach((node) => {
        node.age = year_ - node.birthYear;
        if (
            node.deathYear <= year_ ||
            node.deathYear == null
        )
            node.age = -1;
    })

    return nodes_;
}


function countAges(nodes_, year_) {
    let out = {};
    nodes_
        .filter((n) => n.birthYear != null)
        .filter((n) => n.deathYear != null)
        .filter((n) => n.deathYear > year_)
        .forEach((node) => {
            let age = year_ - node.birthYear;

            if (age < 0)
                return;

            if (out.hasOwnProperty(age)) {
                out[age] += 1;
            } else {
                out[age] = 1;
            }
        });

    return dictToArray(out);
}


// INTERACTION ================================================================

function toggleToolsDisplay() {
    if (tools.css('left') == '0px') {
        tools.css(
            'left',
            0 - tools.width()
        );
    } else {
        tools.css(
            'left',
            '0px'
        );
    }
}


function yearinputChange(inputString) {
    if (
        inputString == '' ||
        isNaN(Number(inputString))
    )
        return;

    var newYear = Number(inputString);

    if (
        newYear >= minYear &&
        newYear <= maxYear
    )
        year = newYear;

    yearDiv.html(year);
}


function goToMinYear() {
    year = minYear;
    yearDiv.html(year);
    refreshGraph = true;
}


function goToMaxYear() {
    year = maxYear;
    yearDiv.html(year);
    refreshGraph = true;
}


function togglePlayPause() {
    isPlaying = !isPlaying;
}


function updateInterestingLastNames(lastNamesString) {
    interestingLastNames = lastNamesString
        .split(' ')
        .filter((i) => i.length > 0);

    nodes = allNodes
    if (interestingLastNames.length > 0)
        nodes = allNodes.filter((node) => 
            interestingLastNames.includes(node.lastName)
        )

    pp.updatePlot(
        calcAges(nodes, year)
    );
}

