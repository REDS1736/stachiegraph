// SETTINGS ====================================================================

const loopDuration = 200;
let nodeInitPosition = {
    x: null,
    y: null,
}


// With config keys .......................................

let interestingLastNames = [];
if (config_interestingLastNames != '')
    interestingLastNames = config_interestingLastNames
        .split(' ')
        .filter((i) => i.length > 0);

let minYear = 1650;
if (config_minYear != null)
    minYear = config_minYear;

let maxYear = 2022;
if (config_maxYear != null)
    maxYear = config_maxYear;

let updateRange = true;
if (config_updateRange != null)
    updateRange = config_updateRange;


// VARIABLES ==================================================================

// Graph ..................................................
let tpp = null;

// Data ...................................................
let allNodes = null;
let nodes = null;

// DOM ....................................................
let canvas = null;
let tools = null;
let year_div = null;
let detailsDiv = null;
let interestingLastNamesDiv = null;
let updateRange_checkbox = null;

// State ..................................................
let isPlaying = true;
let year = null;
let refreshGraph = false;


// MAIN =======================================================================

init();


function init() {
    // Init data ..........................................
    nodeInitPosition = {
        x: window.innerWidth,
        y: window.innerHeight / 2,
    }
    year = minYear;
    allNodes = prepareData(
        transportData,
        nodeInitPosition.x,
        nodeInitPosition.y,
    )
    if (interestingLastNames.length > 0)
        nodes = allNodes.filter((node) => 
            interestingLastNames.includes(node.lastName)
        )

    // Get DOM variables ..................................
    canvas = d3.select('#graph')
    detailsDiv = $('#node-details')
    interestingLastNamesDiv = $('#filter')
    year_div = $('#year')
    tools = $('#tools')
    updateRange_checkbox = $('#updaterange-checkbox')

    // Init DOM ...........................................
    interestingLastNamesDiv.val(interestingLastNames.join(' '))
    updateRange_checkbox.prop('checked', updateRange)

    // Init plot ..........................................
    tpp = TadpolePlot(canvas);
    tpp.setMargin(400, 100);
    tpp.setPixelDimensions(window.innerWidth, window.innerHeight);
    tpp.setData(
        nodes.filter((n) => n.birthYear <= year)
    );
    tpp.setRange(
        d3.min(nodes.map((n) => n.birthYear)),
        d3.max(nodes.map((n) => n.birthYear)),
    )
    let colorPalette = {};
    let paletteFillerFunc = d3.scaleOrdinal(d3.schemeCategory10);
    allNodes
        .map((n) => n.lastName)
        .filter(unique)
        .forEach((ln) => {
            colorPalette[ln] = paletteFillerFunc(ln);
        })
    tpp.setColorPalette(colorPalette);
    tpp.setDetailsDiv(detailsDiv);
    tpp.initPlot();

    // Start loop .........................................
    loop();
    d3.interval(
        loop,
        loopDuration,
        d3.now()
    )
}


function loop() {
    if (isPlaying) {
        lastYear = year;
        year = Math.min(maxYear, year + 1);
        year_div.html(year);
    }

    if (year != lastYear || refreshGraph) {
        let activeNodes = nodes.filter((n) => n.birthYear <= year)

        if (updateRange) {
            tpp.setRange(
                d3.min(activeNodes.map((n) => n.birthYear)),
                d3.max(activeNodes.map((n) => n.birthYear)),
            )
        }
        tpp.updatePlot(activeNodes);

        refreshGraph = false;
    }
}


// UTILITY ====================================================================


// DATA PROCESSING ============================================================

/** * Is `value` unique in `list`?
 *
 * @param {*} value 
 * @param {int} index 
 * @param {*[]} list
 * @returns {boolean}
 */
function unique(value, index, list) { 
    return list.indexOf(value) === index;
}


function date2year(dateString) {
    if (
        dateString == undefined ||
        dateString == null
    )
        return null;
    if (dateString.length < 4)
        return null;

    var out = Number(dateString.substring(0, 4));
    if (isNaN(out))
        return null;
    return out;
}


function prepareData(dataInput, x, y) {
    out = dataInput['nodes'];

    out.forEach((node) => {
        node.birthYear = date2year(node.birthDate);
        node.x = x + (node.birthYear / 10);
        node.y = y;
    });

    out = out.filter((n) => n.birthYear != null);

    return out;
}


// INTERACTION ================================================================

function toggleToolsDisplay() {
    if (tools.css('left') == '0px') {
        tools.css(
            'left',
            0 - tools.width()
        );
    } else {
        tools.css(
            'left',
            '0px'
        );
    }
}


function updateInterestingLastNames(lastNamesString) {
    interestingLastNames = lastNamesString
        .split(' ')
        .filter((i) => i.length > 0);

    nodes = allNodes.filter((node) => 
        interestingLastNames.includes(node.lastName)
    )

    tpp.setRange(
        d3.min(nodes.map((n) => n.birthYear)),
        d3.max(nodes.map((n) => n.birthYear)),
    )
    tpp.updatePlot(
        nodes.filter((n) => n.birthYear <= year)
    );
}


function yearinputChange(inputString) {
    if (
        inputString == '' ||
        isNaN(Number(inputString))
    )
        return;

    var newYear = Number(inputString);

    if (
        newYear >= minYear &&
        newYear <= maxYear
    )
        year = newYear;
}


function goToMinYear() {
    year = minYear;
    refreshGraph = true;
}


function goToMaxYear() {
    year = maxYear;
    refreshGraph = true;
}


function setUpdateRange(val) {
    updateRange = val;
    if (!updateRange) {
        tpp.setRange(
            d3.min(nodes.map((n) => n.birthYear)),
            d3.max(nodes.map((n) => n.birthYear)),
        )
    }
    tpp.setAlphaTarget(1)
}


function togglePlayPause() {
    if (isPlaying) {
        tpp.setAlphaTarget(0)
        isPlaying = false;
    } else {
        tpp.setAlphaTarget(1)
        isPlaying = true;
    }
}
