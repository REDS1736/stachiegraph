// SETTINGS ====================================================================

let width = 700;
let height = 600;
const loopDuration = 200;


// With config keys .......................................

let interestingLastNames = [];
if (config_interestingLastNames != '')
    interestingLastNames = config_interestingLastNames
        .split(' ')
        .filter((i) => i.length > 0);

let minYear = 1950;
if (config_minYear != null)
    minYear = config_minYear;

let maxYear = 2022;
if (config_maxYear != null)
    maxYear = config_maxYear;


// VARIABLES ===================================================================

// Graph ..................................................
let bp = null;

// Data ...................................................
let nodes = null;

// DOM ....................................................
let details = null;
let lastname_list = null;
let lastname_filter = null;
let plot = null;
let year_div = null;

// State ..................................................
let year = null;
let isPlaying = true;


// INIT ========================================================================

init();


function init() {
    // Init data ..........................................
    year = minYear;

    nodes = prepareData(transportData)['nodes'];

    // Get DOM variables ..................................
    details = $('#details');
    lastname_list = $('#lastname-list');
    lastname_filter = $('#lastname-filter');
    plot = d3.select('#graph');
    year_div = $('#year');

    // Init DOM ...........................................
    plot
        .attr('width', width)
        .attr('height', height);

    // Populate lastname-list
    let lnlHtml = '';
    let lastNames = nodes
        .map((n) => n.lastName)
        .filter(unique)
        .filter((i) => i != '')
        .sort();
    lastNames.forEach((lastName) => {
        lnlHtml += '<li>';
        lnlHtml += lastName;
        lnlHtml += '</li>';
    })
    lastname_list.html(lnlHtml);

    lastname_filter.val(
        interestingLastNames.join(' ')
    );

    // Init plot ..........................................
    bp = new BarPlot(plot);
    bp.setMargin(70, 100, 70, 70);
    bp.setPixelDimensions(width, height);
    bp.setDomainX(interestingLastNames)
    bp.setDomainY(
        0,
        d3.max(
            calcMemberCountData(nodes, maxYear)
                .filter((d) => interestingLastNames.includes(d.x))
                .map((d) => d.y)
        ),
    );
    bp.setData(calcMemberCountData(nodes, year));
    bp.setBarMouseOver(barMouseOver);
    bp.setBarMouseOut(barMouseOut);
    bp.initPlot();

    // Start loop .........................................
    loop();
    d3.interval(
        loop,
        loopDuration,
        d3.now()
    );
}


function loop() {
    if (isPlaying) {
        year = Math.min(maxYear, year + 1);
        year_div.html(year);
    }

    bp.setData(
        calcMemberCountData(
            nodes,
            year,
        )
    )

    bp.updatePlot();
}


// UTILITY =====================================================================

function date2year(dateString) {
    if (
        dateString == undefined ||
        dateString == null
    )
        return null;
    if (dateString.length < 4)
        return null;

    var out = Number(dateString.substring(0, 4));
    if (isNaN(out))
        return null;
    return out;
}


/**
 * Is `value` unique in `list`?
 *
 * @param {*} value 
 * @param {int} index 
 * @param {*[]} list
 * @returns {boolean}
 */
function unique(value, index, list) { 
    return list.indexOf(value) === index;
}


/**
 * Translate object of shape `{ a: aa, b: bb, c: cc }` into array of shape
 * `[{ x: a, y: aa }, { x: b, y: bb }, { x: c, y: cc }]`.
 * 
 * @param {object} dict
 * @returns {object[]}
 */
function dictToArray(dict) {
    let out = [];
    Object.keys(dict).forEach((key) => {
        out.push({
            'x': key,
            'y': dict[key]
        })
    })
    return out.sort((a, b) => a.x > b.x);
}


// DATA PROCESSING =============================================================

function prepareData(dataInput) {
    out = dataInput;

    //out['nodes'] = out['nodes'].slice(0, 10);

    out['nodes'].forEach((node) => {
        node.birthYear = date2year(node.birthDate);
        node.deathYear = date2year(node.deathDate);

        node.lifeDuration = null;
        if (node.birthYear != null && node.deathYear != null)
            node.lifeDuration = node.deathYear - node.birthYear;
    });

    return out;
}


/**
 * Count: How many members of each family (as identified per lastName) have been
 * born at the time of `year`?
 * Count is 0 for families not specified in `interestingLastNames_`.
 * Return member counts for all last names (no filtering) if
 * `interestingLastNames_` is an empty array.
 * 
 * @param {object[]} nodes_
 * @param {number} year_
 * @param {string[]} interestingLastNames_
 * @returns {object[]}
 */
function calcMemberCountData(
    nodes_,
    year_,
) {
    let out = {};
    nodes_.forEach((node) => {
        // Missing data
        if (
            node.lastName == '' ||
            node.lastName == undefined ||
            node.lastName == null ||
            node.birthYear == null
        ) {
            return;
        }


        // Has not been born yet
        if (node.birthYear > year_) {
            if (!out.hasOwnProperty(node.lastName))
                out[node.lastName] = 0;

            return;
        }

        // Actually counts into memberCount
        if (out.hasOwnProperty(node.lastName)) {
            out[node.lastName] += 1;
        } else {
            out[node.lastName] = 1;
        }
    });

    return dictToArray(out);
}


// INTERACTION =================================================================

function updateInterestingLastNames(lastNamesString) {
    interestingLastNames = lastNamesString
        .split(' ')
        .filter((i) => i.length > 0);

    bp.setDomainX(interestingLastNames)

    let maxMemberCount = d3.max(calcMemberCountData(
        nodes.filter((d) => interestingLastNames.includes(d.lastName)),
        maxYear
    ).map((d) => d.y))
    bp.setDomainY(0, maxMemberCount);
}


function goToMinYear() {
    year = minYear;
    year_div.html(year);

    // If `loopDuration` is small enough, this section is not strictly
    // necessary... but if `loopDuration` is 3000, it might take up to 3
    // seconds for the click to change the plot's year, so this section
    // takes care of that by always changing the plot's year instantly.
    bp.setData(calcMemberCountData(
        nodes,
        year,
    ))
    bp.updatePlot();
}


function goToMaxYear() {
    year = maxYear;
    year_div.html(year);

    // If `loopDuration` is small enough, this section is not strictly
    // necessary... but if `loopDuration` is 3000, it might take up to 3
    // seconds for the click to change the plot's year, so this section
    // takes care of that by always changing the plot's year instantly.
    bp.setData(calcMemberCountData(
        nodes,
        year,
    ))
    bp.updatePlot();
}


function yearinputChange(inputString) {
    if (
        inputString == '' ||
        isNaN(Number(inputString))
    )
        return;

    var newYear = Number(inputString);

    if (
        newYear >= minYear &&
        newYear <= maxYear
    )
        year = newYear;

    year_div.html(year);

    // If `loopDuration` is small enough, this section is not strictly
    // necessary... but if `loopDuration` is 3000, it might take up to 3
    // seconds for the click to change the plot's year, so this section
    // takes care of that by always changing the plot's year instantly.
    bp.setData(calcMemberCountData(
        nodes,
        year,
    ))
    bp.updatePlot();
}


function togglePlayPause() {
    isPlaying = !isPlaying;
}


function barMouseOver(event) {
    let bar = event.target.getBoundingClientRect();
    details.css({
        'display': 'block',
        'left': bar.left,
        'top': bar.top - 20,
        'width': bar.width,
    });
    details.html(Math.round($(event.target).attr('raw')));
}


function barMouseOut(event) {
    details.css('display', 'none');
}
