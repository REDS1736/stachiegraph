// SETTINGS ====================================================================

const mouseTouchRadius = 40;
var alphaTarget = 1;

var spawnOffset = 10;
var panSpeed = 10;
var zoomSpeed = 1;


// Settings which already have config.json / getparam keys ..........

var filterStr = config_defaultFilter;
if (getparam_filterString != '')
    filterStr = getparam_filterString

var minYear = 1950;
if (config_minYear != null)
    minYear = config_minYear;

var maxYear = 2022;
if (config_maxYear != null)
    maxYear = config_maxYear;

var year = minYear;
if (getparam_year != null)
    year = getparam_year;

var isPlaying = true;
if (getparam_isPlaying != null)
    isPlaying = getparam_isPlaying;

var showDead = true;
if (config_showDead != null)
    showDead = config_showDead;

var maxLifeTime = 120;
if (config_maxLifeTime != null)
    maxLifeTime = config_maxLifeTime;

var zoomLevel = 36;
if (config_defaultZoomLevel != null)
    zoomLevel = config_defaultZoomLevel;

var nodeRadius = 5;
if (config_nodeRadius != null)
    nodeRadius = config_nodeRadius;

var linkDistance = 30;
if (config_linkDistance != null)
    linkDistance = config_linkDistance;

var yearsPerSecond = 5;
if (config_yearsPerSecond != null)
    yearsPerSecond = config_yearsPerSecond;

var nodeBgColor = '';
if (config_nodeBgColor != null)
    nodeBgColor = config_nodeBgColor;

var nodeBgMaxAge = 20;
if (config_nodeBgMaxAge != null)
    nodeBgMaxAge = config_nodeBgMaxAge;

var nodeBgRadius = 15;
if (config_nodeBgRadius != null)
    nodeBgRadius = config_nodeBgRadius;

var nodesColorPaletteDisplayOnlyActive = true;
if (config_nodesColorPaletteDisplayOnlyActive != null)
    nodesColorPaletteDisplayOnlyActive =
        config_nodesColorPaletteDisplayOnlyActive;

var graphBgColor = '#ffffff';
if (config_graphBgColor != null)
    graphBgColor = config_graphBgColor;
if (getparam_graphBgColor != null)
    graphBgColor = getparam_graphBgColor;

var nodesColorPalette = null;
if (getparam_nodesColorPalette != null)
    nodesColorPalette = getparam_nodesColorPalette;

var linksColorPalette = null;
if (getparam_linksColorPalette != null)
    linksColorPalette = getparam_linksColorPalette;


// DOM =========================================================================

var canvas = null;
var context = null;
var width = null;
var height = null;


// VARIABLES ===================================================================
var dataActive = {};
var dataFull = {};
var refreshGraph = false;

var simulation = null;
var users = [];
var interval = null;

var graphCenter = {
    x: 0,
    y: 0
};

// MAIN ========================================================================

/**
 * 1. Init DOM variables
 * 2. Init other stuff
 */
function init() {
    // DOM variables ......................................
    width = window.innerWidth;
    height = window.innerHeight;

    canvas = d3.select('canvas#graph');
    canvas
        .attr("width", width)
        .attr("height", height);

    context = d3.select('canvas#graph')
        .node()
        .getContext('2d');
    context.translate(
        width / 2,
        height / 2
    );

    // Other stuff ........................................
    setFilter(filterStr);
    displayYear(year);

    dataFull = pointLinksToObjects(transportData);

    dataActive = selectActiveData(
        dataFull,
        year,
        showDead,
        filterStr
    );

    initNodesColorPalette(dataFull.nodes);
    $('#ncp-activecheckbox').attr('checked', nodesColorPaletteDisplayOnlyActive);

    initLinksColorPalette(dataFull.links);

    initBgColorpicker(graphBgColor);

    simulation = getTreeSimulation(dataActive);

    interval = d3.interval(
        loop,
        1000 / yearsPerSecond,
        d3.now()
    );

    window.onkeydown = function(event){
        switch (event.key) {
            case 'ArrowUp':
                panGraph(0, -panSpeed);
                break;
            case 'ArrowDown':
                panGraph(0, panSpeed);
                break;
            case 'ArrowLeft':
                panGraph(-panSpeed, 0);
                break;
            case 'ArrowRight':
                panGraph(panSpeed, 0);
                break;
            case 'j':
                zoomGraph(-zoomSpeed);
                break;
            case 'k':
                zoomGraph(zoomSpeed);
                break;
        }
    };
}


// FUNCTIONS ===================================================================

// Data handling ----------------------------------------------------
 
/**
 * Extracts year from a full date in 'yyyy-mm-dd...' format.
 * 
 * READ     -
 * WRITE    -
 * CALL     -
 * 
 * @param {string} dateString Full date as a string
 * @returns {number} null if no year could be extracted
 */
function getYearFromDate(dateString) {
    var out = dateString.substring(0, 4);
    if (!isNaN(Number(out)))
        return Number(out);
    return null;
}


/**
 * Return the subset of `nodes` which matches the filter specified via
 * `filterString` and `inFilter()`.
 * 
 * READ     -
 * WRITE    -
 * CALL
 *      inFilter
 * 
 * @param {object[]} nodes List of nodes to filter
 * @param {string} filterString e.g. "Skywalker Palpatine"
 * @returns {object[]} Subset of `nodes`
 */
function filterNodes(
    nodes,
    filterString
) {
    var filterItems = filterString.split(' ');
    filterItems = filterItems.filter(function (i) {
        return i.length > 0;
    });

    var nodesOutput = [];
    for (var i = 0; i < nodes.length; i++) {
        if (inFilter(nodes[i], filterItems)) {
            nodesOutput.push(nodes[i]);
        }
    }

    return nodesOutput;
}


/**
 * Get the first item from `nodes` where the `id` attribute matches `id`.
 * Return null if no node with this id is found.
 * 
 * READ     -
 * WRITE    -
 * CALL     -
 * 
 * @param {string} id
 * @param {object[]} nodes
 * @returns {(object|null)}
 */
function getNodeById(
    id,
    nodes
) {
    for (var i = 0; i < nodes.length; i++) {
        if (nodes[i].id == id) return nodes[i];
    }
    return null;
}


/**
 * Does this `node` match the filter specified by `filterItems`?
 * 
 * READ     -
 * WRITE    -
 * CALL     -
 * 
 * @param {object} node
 * @param {string[]} filterItems
 * @returns {boolean}
 */
function inFilter(
    node,
    filterItems
) {
    if (filterItems.length == 0)
        return true;

    for (var i = 0; i < filterItems.length; i++) {
        if (node.name.includes(filterItems[i])) {
            return true;
        }
    }

    return false;
}


/**
 * Are both endpoints of this link `link` defined as nodes in `nodes`?
 * 
 * READ     -
 * WRITE    -
 * CALL
 *      getNodeById
 * 
 * @param {object} node
 * @param {string[]} filterItems
 * @returns {boolean}
 */
function linkIsValid(
    link,
    nodes
) {
    if (
        getNodeById(link.source, nodes) &&
        getNodeById(link.target, nodes)
    )
        return true;

    return false;
}


/**
 * Increment `yearInput` depending on `isPlaying`.
 * 
 * READ
 *      isPlaying
 *      maxYear
 * WRITE    -
 * CALL     -
 * 
 * @param {number} yearInput
 * @returns {number}
 */
function advanceYear(yearInput) {
    if (isPlaying) {
        return Math.min(
            yearInput += 1,
            maxYear
        );
    } else {
        return yearInput;
    }
}


/**
 * Return subset of `dataInput` which matches the following criteria:
 * 1. Has been born at time `_year`
 * 2. Is still alive at time `_year` (if `_showDead` is false)
 * 3. Matches filter specified in `filterStr`
 * 
 * READ     -
 * WRITE    -
 * CALL
 *      filterNodes
 *      selectActiveNodes
 *      selectActiveLinks
 * 
 * @param {object} dataInput
 * @param {number} _year
 * @param {boolean} _showDead
 * @param {string} filterStr
 * @returns {object}
 */
function selectActiveData(
    dataInput,
    _year,
    _showDead,
    filterStr
) {
    dataOutput = {
        nodes: [],
        links: []
    }

    var nodes = filterNodes(dataInput.nodes, filterStr);

    dataOutput.nodes = selectActiveNodes(
        nodes,
        _year,
        _showDead
    );

    dataOutput.links = selectActiveLinks(
        dataInput.links,
        dataOutput.nodes,
    );

    return dataOutput;
}


/**
 * Return subset of `nodes` which matches the following criteria:
 * 1. Has been born at time `_year`
 * 2. Is still alive at time `_year` (if `_showDead` is false)
 * 3. Is it not older than `maxLifeTime` (if its death date is unknown)
 * 
 * READ
 *      maxLifeTime
 * WRITE    -
 * CALL     -
 * 
 * @param {object[]} nodes
 * @param {number} _year
 * @param {boolean} _showDead
 * @returns {object[]}
 */
function selectActiveNodes(
    nodes,
    _year,
    _showDead
) {
    var nodesOutput = [];
    nodes.forEach(function (node) {
        if (node.birthDate != null) {
            var birthYear = getYearFromDate(node.birthDate);
            if (
                nodesOutput.indexOf(node) == -1 &&
                birthYear < _year
            ) {
                if (showDead) {
                    nodesOutput.push(node);
                } else {
                    if (node.deathDate != null) {
                        let deathDate = getYearFromDate(node.deathDate);
                        if (deathDate > _year) {
                            nodesOutput.push(node);
                        }
                    } else {
                        if ((_year - birthYear) <= maxLifeTime) {
                            nodesOutput.push(node);
                        }
                    }
                }
            }
        }
    });
    return nodesOutput;
}


/**
 * Return subset of `links` which only references nodes from `nodes`.
 * 
 * READ     -
 * WRITE    -
 * CALL     -
 * 
 * @param {object} link
 * @param {object[]} nodes
 * @returns {object[]}
 */
function selectActiveLinks(
    links,
    nodes
) {
    var linksOutput = [];

    links.forEach(function (link) {
        if (
            nodes.indexOf(link.source) != -1 &&
            nodes.indexOf(link.target) != -1
        ) {
            linksOutput.push(link);
        }
    });

    return linksOutput;
}


/**
 * Return `data` but the `links` attribute has been modified so that * each of
 * its elements' `source` and `target` attributes contain node objects instead
 * of strings pointing at the `id` attributes of node objects.
 * This transformation is necessary for d3.js to correctly simulate link forces.
 * 
 * READ     -
 * WRITE    -
 * CALL     -
 * 
 * @param {object} data
 * @returns {object}
 */
function pointLinksToObjects(
    data,
) {
    var links = [];
    var dataOut = data;

    data.links.forEach(function (link) {
        links.push(link);
        links[links.length - 1].source = getNodeById(
            link.source,
            data.nodes
        )
        links[links.length - 1].target = getNodeById(
            link.target,
            data.nodes
        )
    });

    dataOut.links = links;

    return dataOut;
}


// Simulation -------------------------------------------------------

/**
 * Create the tree graph simulation and return it.
 * 
 * READ
 *      zoomLevel
 *      graphCenter
 *      linkDistance
 *      alphaTarget
 * WRITE    -
 * CALL
 *      getDragTargetNode
 *      onDragDrag
 *      onDragEnd
 *      onDragStart
 *      onMouseMove
 *      treeTicked
 * 
 * @param {object} data
 * @returns {object}
 */
function getTreeSimulation(data) {
    canvas
        .on('mousemove', onMouseMove)
        .call(d3.drag()
            .container(document.querySelector('canvas#graph'))
            .subject(getDragTargetNode)
            .on('start', onDragStart)
            .on('drag', onDragDrag)
            .on('end', onDragEnd));

    treeSimulation = d3.forceSimulation(data.nodes)
        .force('charge', d3.forceManyBody().strength(
            zoomLevelToZoomVal(zoomLevel)
        ))
        .force('centering', d3.forceCenter(graphCenter.x, graphCenter.y))
        .force('link', d3.forceLink(data.links).distance(linkDistance).strength(0.5))
        .force('x', d3.forceX())
        .force('y', d3.forceY())
        .alphaTarget(alphaTarget)
        .on('tick', treeTicked);

    return treeSimulation;
}


/**
 * This function is called on every tick of the tree simulation.
 * It re-draws everything on the canvas.
 * 
 * READ
 *      context
 *      dataActive
 *      height
 *      nodeBgColor
 *      width
 * WRITE
 *      context
 * CALL
 *      drawLink
 *      drawNode
 *      drawNodeBg
 * 
 * @returns {null}
 */
function treeTicked() {
    context.rect(
        -width / 2,
        -height / 2,
        width,
        height,
    );
    context.fillStyle = graphBgColor;
    context.fill();

    context.save();
    if (nodeBgColor != '') {
        dataActive.nodes.forEach(function (node) {
            drawNodeBg(node);
        });
    }
    dataActive.links.forEach(function (link) {
        drawLink(link);
    })
    dataActive.nodes.forEach(function (node) {
        drawNode(node);
    });
    context.restore();
}


/**
 * This function is called `yearsPerSecond` times per second.
 * It updates the `year` and the graph / simulation.
 * 
 * READ
 *      alphaTarget
 *      dataFull
 *      linkDistance
 *      refreshGraph
 *      showDead
 *      year
 * WRITE
 *      dataActive
 *      filterStr
 *      refreshGraph
 *      simulation
 *      year
 * CALL
 *      advanceYear
 *      updateNodesColorPaletteDisplay
 * 
 * @returns {null}
 */
function loop() {
    var oldYear = year;
    year = advanceYear(year);
    displayYear(year);

    if (year != oldYear) refreshGraph = true;

    if (refreshGraph) {
        dataActive = selectActiveData(
            dataFull,
            year,
            showDead,
            filterStr
        );
        updateNodesColorPaletteDisplay();

        simulation
            .nodes(dataActive.nodes)
            .force('link', d3.forceLink(dataActive.links).distance(linkDistance).strength(0.5))
            .alphaTarget(alphaTarget)
            .restart();
    }
    refreshGraph = false;
}


// Drawing / UI -----------------------------------------------------

/**
 * Draw `node` on the canvas.
 * 
 * READ
 *      nodeRadius
 *      nodesColorPalette
 * WRITE    -
 * CALL
 *      drawSymbol
 * 
 * @param {object} node
 * @returns {null}
 */
function drawNode(node) {
    if (node.gender == 'M') {
        drawSymbol(
            'square',
            node.x,
            node.y,
            nodeRadius,
            nodesColorPalette[node.lastName]
        );
    } else {
        drawSymbol(
            'circle',
            node.x,
            node.y,
            nodeRadius,
            nodesColorPalette[node.lastName]
        );
    }
}


/**
 * Draw background for `node` on the canvas.
 * 
 * READ
 *      nodeBgMaxAge
 *      nodeBgColor
 *      nodeBgRadius
 *      year
 * WRITE    -
 * CALL
 *      calculateAge
 *      drawSymbol
 *      intToPaddedHex
 * 
 * @param {object} node
 * @returns {null}
 */
function drawNodeBg(node) {
    // Only people who have alredy been born...
    var age = calculateAge(node, year);
    if (age <= 0)
        return;

    // No background if person is older than `recencyMaxAge`
    var colval = parseInt((1 - (age / nodeBgMaxAge)) * 255);
    if (colval <= 0)
        return;

    var appendixHex = intToPaddedHex(colval, 2);
    var color = nodeBgColor + appendixHex;
    drawSymbol(
        'circle',
        node.x,
        node.y,
        nodeBgRadius,
        color
    );
}


/**
 * Convert decimal integer into left-padded hexadecimal integer.
 * 
 * Examples (for `length` = 2):
 *      5  -> 05
 *      10 -> 0a
 *      20 -> 14
 * 
 * @param {number} int
 * @param {number} length
 * @returns {string}
 */
function intToPaddedHex(int, length) {
    var hex = int.toString(16);
    while (hex.length < length) 
        hex = '0' + hex;
    return hex;
}


/**
 * How old is this person at the time of `_year`?
 * 
 * @param {object} node
 * @param {number} _year
 * @returns {number}
 */
function calculateAge(node, _year) {
    var birthYear = getYearFromDate(node.birthDate);
    if (birthYear == null)
        return null;
    return _year - birthYear;
}


/**
 * Draw `link` on the canvas.
 * 
 * READ     -
 * WRITE
 *      context
 * CALL     -
 * 
 * @param {object} link
 * @returns {null}
 */
function drawLink(link) {
    context.beginPath();
    context.lineWidth = 1;
    context.strokeStyle = linksColorPalette[link.relation];
    context.moveTo(link.source.x, link.source.y);
    context.lineTo(link.target.x, link.target.y);
    context.stroke();
}


/**
 * Utility function: Draw a defined symbol on the canvas.
 * 
 * READ
 * WRITE
 *      context
 * CALL
 * 
 * @param {string} symbol 'circle' or 'square'
 * @param {number} x x-coordinate of the center of the symbol
 * @param {number} y y-coordinate of the center of the symbol
 * @param {number} radius radius of the symbol
 * @param {string} color fill color of the symbol
 * @returns {null}
 */
function drawSymbol(
    symbol,
    x,
    y,
    radius,
    color
) {
    context.beginPath();
    switch(symbol) {
        case 'circle':
            context.arc(
                x,
                y,
                radius,
                0, 2 * Math.PI
            );
            break;
        case 'square':
            context.rect(
                x - radius,
                y - radius,
                radius * 2,
                radius * 2
            )
            break;
        default:
            break;
    }
    context.fillStyle = color;
    context.fill();
}


/**
 * Update the DOM element for displaying the current year.
 * 
 * READ     -
 * WRITE
 *      $('#year')
 * CALL     -
 * 
 * @param {number} yearInput Display this year
 * @returns {null}
 */
function displayYear(yearInput) {
    $('#year').text(yearInput);
}


/**
 * Set the value of `filterStr` and the DOM filter input element to `value`.
 * 
 * READ     -
 * WRITE
 *      filterStr
 *      $('#filter')
 * CALL     -
 * 
 * @param {string} value
 * @returns {null}
 */
function setFilter(value) {
    filterStr = value;
    $('#filter').val(value);
}


/**
 * Initialize the nodes color palette DOM elements:
 * 1. Create one color input per unique lastName in `nodes`.
 * 2. Call ´updateNodesColorPaletteDisplay()´.
 * 
 * READ     -
 * WRITE
 *      nodesColorPalette
 *      $('#nodes-colpal-container')
 * CALL
 *      updateNodesColorPaletteDisplay
 * 
 * @param {object[]} nodes
 * @returns {null}
 */
function initNodesColorPalette(nodes) {
    if (nodesColorPalette == null) {
        nodesColorPalette = {};
        var paletteFillerFunc = d3.scaleOrdinal(d3.schemeCategory10);
        nodes.forEach(function(node) {
            nodesColorPalette[node.lastName] = paletteFillerFunc(node.lastName);
        });
    }

    var html = '';
    Object.keys(nodesColorPalette).forEach(function(lastName, i) {
        html += '<div class="colpal-element"';
        html += '   lastname="' + lastName + '">';
        html += '   <input type="color"';
        html += '       name="nodes-colpal-input-' + i + '"';
        html += '       value="' + nodesColorPalette[lastName] + '"';
        html += '       onchange="updateNodesColorPalette(\'' + lastName + '\', this.value);">';
        html += '   <label for="nodes-colpal-input-' + i + '">';
        html += '       ' + lastName;
        html += '   </label>';
        html += '</div>';
    });

    $('#nodes-colpal-container').html(html);

    updateNodesColorPaletteDisplay();
}


/**
 * Update the `lastName` value of `nodesColorPalette` to `color`.
 * Force refresh of graph.
 * 
 * READ     -
 * WRITE
 *      nodesColorPalette
 *      refreshGraph
 * CALL     -
 * 
 * @param {string} lastName
 * @param {string} color
 * @returns {null}
 */
function updateNodesColorPalette(lastName, color) {
    nodesColorPalette[lastName] = color;
    refreshGraph = true;
}


/**
 * Update which DOM color inputs for the node colors are displayed.
 * 
 * READ
 *      nodesColorPaletteDisplayOnlyActive
 *      dataActive
 * WRITE
 *      $('#nodes-colpal-container .colpal-element')
 * CALL     -
 * 
 * @returns {null}
 */
function updateNodesColorPaletteDisplay() {
    if (nodesColorPaletteDisplayOnlyActive) {
        $('#nodes-colpal-container .colpal-element').each(function() {
            if (valueIsInNodes(
                $(this).attr('lastname'),
                'lastName',
                dataActive.nodes
            )) {
                $(this).css('display', 'block');
            } else {
                $(this).css('display', 'none');
            }
        });
    } else {
        $('#nodes-colpal-container .colpal-element').each(function() {
            $(this).css('display', 'block');
        });
    }
}


/**
 * Is there a node in `nodes` for which the attribute `attribute` contains the
 * value `value`?
 * 
 * READ     -
 * WRITE    -
 * CALL     -
 * 
 * @param {any} value
 * @param {string} attribute
 * @param {object[]} nodes
 * @returns {boolean}
 */
function valueIsInNodes(value, attribute, nodes) {
    for (var i = 0; i < nodes.length; i++) {
        if (nodes[i][attribute] == value)
            return true;
    }
    return false;
}


/**
 * OnChange listener attached in tree.html.
 * Fired when the nodes color palette checkbox is clicked.
 * 
 * READ     -
 * WRITE
 *      nodesColorPaletteDisplayOnlyActive
 * CALL
 *      updateNodesColorPaletteDisplay
 * 
 * @param {any} value
 * @param {string} attribute
 * @param {object[]} nodes
 * @returns {boolean}
 */
function nodesColorPaletteCheckboxOnChange(checked) {
    nodesColorPaletteDisplayOnlyActive = checked;
    updateNodesColorPaletteDisplay();
}


/**
 * Initialize the links color palette DOM elements:
 * 1. Create one color input per unique relation in `links`.
 * 2. Call ´updateLinksColorPaletteDisplay()´.
 * 
 * READ     -
 * WRITE
 *      linksColorPalette
 *      $('#links-colpal-container')
 * CALL     -
 * 
 * @param {object[]} links
 * @returns {null}
 */
function initLinksColorPalette(links) {
    if (linksColorPalette == null) {
        linksColorPalette = {};
        var paletteFillerFunc = d3.scaleOrdinal(d3.schemeCategory10);
        links.forEach(function(link) {
            linksColorPalette[link.relation] = paletteFillerFunc(link.relation);
        });
    }

    var html = '';
    Object.keys(linksColorPalette).forEach(function(relation, i) {
        html += '<div class="colpal-element"';
        html += '   relation="' + relation + '">';
        html += '   <input type="color"';
        html += '       name="links-colpal-input-' + i + '"';
        html += '       value="' + linksColorPalette[relation] + '"';
        html += '       onchange="updateLinksColorPalette(\'' + relation + '\', this.value);">';
        html += '   <label for="links-colpal-input-' + i + '">';
        html += '       ' + relation;
        html += '   </label>';
        html += '</div>';
    });

    $('#links-colpal-container').html(html);
}


/**
 * Update the `relation` value of `linksColorPalette` to `color`.
 * Force refresh of graph.
 * 
 * READ     -
 * WRITE
 *      linksColorPalette
 *      refreshGraph
 * CALL     -
 * 
 * @param {string} relation
 * @param {string} color
 * @returns {null}
 */
function updateLinksColorPalette(relation, color) {
    linksColorPalette[relation] = color;
    refreshGraph = true;
}


function initBgColorpicker(bgcol) {
    $('#colorpicker-bg').val(bgcol);
}


function setBgColor(color) {
    graphBgColor = color;
    refreshGraph = true;
}


// Interaction ------------------------------------------------------

function getDragTargetNode(event) {
    return simulation.find(
        event.x - width / 2,
        event.y - height / 2,
        mouseTouchRadius
    )
}


function onDragStart(event) {
    if (!event.active) {
        simulation
            .alphaTarget(0.3)
            .restart();
    }
    event.subject.fx = event.subject.x;
    event.subject.fy = event.subject.y;
}


function onDragDrag(event) {
    event.subject.fx = event.x;
    event.subject.fy = event.y;
}


function onDragEnd(event) {
    if (!event.active)
        simulation.alphaTarget(0);
    event.subject.fx = null;
    event.subject.fy = null;
}


function onMouseMove(event) {
    var mouse = d3.pointer(event);
    var node = simulation.find(
        mouse[0] - width / 2,
        mouse[1] - height / 2,
        mouseTouchRadius
    );
    if (!node) hideNodeDetails();
    else displayNodeDetails(node, mouse)
}


function hideNodeDetails() {
    d3.selectAll('#node-details')
        .style('display', 'none')
}


function displayNodeDetails(node, mouse) {
    d3.selectAll('#node-details')
        .style('display', 'block')
        .style('left', (mouse[0] + 20) + 'px')
        .style('top', (mouse[1] - 20) + 'px');
    d3.select('#node-details-name').html(node.name);
    d3.select('#node-details-birthdate').html(
        node.birthDate
    );
}


function updateFilter(inputString) {
    filterStr = inputString;
    refreshGraph = true;
}


/**
 * Attached to "GO"-button in tree.html via `onclick` attribute.
 */
function yearinputChange(inputString) {
    if (
        inputString == '' ||
        isNaN(Number(inputString))
    )
        return;

    var newYear = Number(inputString);

    if (
        newYear >= minYear &&
        newYear <= maxYear
    )
        year = newYear;

    refreshGraph = true;
}


function goToMinYear() {
    year = minYear;
    refreshGraph = true;
}


function goToMaxYear() {
    year = maxYear;
    refreshGraph = true;
}


function togglePlayPause() {
    isPlaying = !isPlaying;
}


function panGraph(x, y) {
    graphCenter.x += x;
    graphCenter.y += y;

    simulation.force('centering', d3.forceCenter(
        graphCenter.x,
        graphCenter.y
    ));
}


function zoomLevelToZoomVal(zl) {
    return -Math.pow(1.15, zl);
}


function zoomGraph(z) {
    zoomLevel = Math.max(
        zoomLevel + z,
        0
    );
    simulation
        .force('charge', d3.forceManyBody().strength(
            zoomLevelToZoomVal(zoomLevel)
        ))
        .alphaTarget(alphaTarget);
}


/**
 * Toggle display of the tools UI menu.
 * 
 * READ     -
 * WRITE    -
 * CALL     -
 * 
 * @returns {null}
 */
function toggleToolsDisplay() {
    var tools = $('#tools');

    if (tools.css('left') == '0px') {
        tools.css(
            'left',
            0 - tools.width()
        );
    } else {
        tools.css(
            'left',
            '0px'
        );
    }
}


init();
