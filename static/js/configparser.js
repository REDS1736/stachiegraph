function ConfigParser() {
    function parser() {};

    parser.json2object = function(jsonString) {
        out = null;
        try {
            out = JSON.parse(jsonString);
        } catch {}
        return out;
    }

    parser.str2pybool = (string) => {
        switch (string) {
            case 'True':
                return true;
            case  'False':
                return false;
            default:
                return null;
        }
    }

    parser.str2jsbool = (string) => {
        switch (string) {
            case 'true':
                return true;
            case  'false':
                return false;
            default:
                return null;
        }
    }

    parser.str2number = function(string) {
        var out = null;
        if (!(
            string == '' ||
            isNaN(Number(string))
        )) {
            out = Number(string);
        }
        return out;
    }

    parser.str2hexcolor = function(string, hasAlpha) {
        if (string[0] != '#')
            return null;

        var int = parseInt(string.substring(1), 16);
        if (hasAlpha) {
            if (parseInt('00000000', 16) <= int <= parseInt('ffffffff', 16))
                return string;
        } else {
            if (parseInt('000000', 16) <= int <= parseInt('ffffff', 16))
                return string;
        }

        return null;
    }

    parser.str2colpal = (str)=> {
        if (
            str == '' ||
            str == undefined ||
            str == null
        )
            return null;

        var out = {};
        var pairs = str.split(',');

        pairs.forEach((pair) => {
            var key = pair.split(':')[0];
            var color = pair.split(':')[1];
            out[key] = color;
        });

        return out;
    }

    return parser;
}